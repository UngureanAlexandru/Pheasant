#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

int main()
{
	struct sockaddr_in clientAddress;
	int clientSocket;
	int result = 0;
	char command[100];

	int port = 3000;
	char* ip = "127.0.0.1";

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);

	clientAddress.sin_family = AF_INET;
	clientAddress.sin_addr.s_addr = inet_addr(ip);
	clientAddress.sin_port = htons(port);

	if (connect(clientSocket, (struct sockaddr *) &clientAddress, sizeof(struct sockaddr)) < 0)
	{
		printf("Error at connect !\n");
		exit(0);
	}

	while(1)
	{
		char message[100];
		char word[100];
		int validWord = -1;

		if (read(clientSocket, message, 100) < 0)
		{
			printf("Can't read data from server !\n");
		}
		printf("Message: %s\n", message);

		if (strlen(message) > 55) 
		{
			char letter;
			printf("Choose a letter: ");
			scanf("%s", &letter);
			printf("\n");

			printf("Choose a word: ");
			scanf("%s", word);
			printf("\n");

			while (word[0] != letter)
			{
				printf("Please choose a word that have as first letter, the letter you selected earlier: ");
				scanf("%s", word);
				printf("\n");
			}

			if (write(clientSocket, &letter, sizeof(char)) < 0)
			{
				printf("Can't send data to server !\n");
			}

			if (write(clientSocket, word, 100) < 0)
			{
				printf("Can't send data to server !\n");
			}

			if (read(clientSocket, &validWord, sizeof(int)) < 0)
			{
				printf("Can't read data from server !\n");
			}

			if (validWord == 0)
			{
				close(clientSocket);
				exit(0);
			}
			else
			{
				printf("Valid word\n");
			}
		} 
		else if (strcmp(message, "Choose a word !") == 0)
		{
			char last2[3];
			last2[0] = ' ';
			last2[1] = ' ';
			last2[2] = ' ';

			int validWord = -1;

			if (read(clientSocket, last2, sizeof(char) * strlen(last2)) < 0)
			{
				printf("Can't read data from server !\n");
			}

			printf("Last 2 letters are %c and %c.\n", last2[0], last2[1]);

			printf("Choose a word: ");
			scanf("%s", word);
			printf("\n");

			if (write(clientSocket, word, 100) < 0)
			{
				printf("Can't send data to server !\n");
			}

			if (read(clientSocket, &validWord, sizeof(int)) < 0)
			{
				printf("Can't read data from server !\n");
			}

			if (validWord == 0)
			{
				printf("You lost !\n");
				close(clientSocket);
				exit(0);
			}
			else if (validWord == 1)
			{
				printf("Valid word\n");
			}
		}
		else if (strcmp(message, "You won !") == 0)
		{
			close(clientSocket);
			exit(0);
		}

	}

	return 0;
}