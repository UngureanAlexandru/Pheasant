#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <string.h>
#include <fcntl.h>

void *userThread(void *params);

char fromConfigFile[10];
int playersNumber;
int serverSocket;

struct sockaddr_in serverAddress;
struct sockaddr_in clientAddress;

int nonFullThread = 0;

int main()
{
	int port = 3000;

	int file = open("config.txt", O_RDONLY);

	if (file < 0)
	{
		printf("Can't open the config file !\n");
	}

	if (read (file, &fromConfigFile, 10) < 0)
	{
		printf("Can't read from the config file !\n");
	}

	playersNumber = atoi(fromConfigFile);

	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	int option = 1;
	setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, (void *) &option, sizeof (option));

	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(port);


	if (bind (serverSocket, (struct sockaddr *) &serverAddress, sizeof(struct sockaddr)) == -1)
	{
		printf("Can't bind !\n");
	}

	while(1)
	{

		if (nonFullThread == 0)
		{	
			nonFullThread = 1;
			pthread_t tempUserThread;
			pthread_create(&tempUserThread, NULL, (void*) userThread, NULL);
		}
	}

	return 0;
}

void *userThread(void *params)
{
	FILE *dictionar = fopen("dictionar.txt", "r");

	int players = 0;
	int playerIDs[playersNumber];
	int playerTurn = 0;
	int startOfTheGame = 1;
	char word[100];

	if (listen(serverSocket, playersNumber) == -1)
	{
		printf("Can't listen !\n");
	}


	while(players < playersNumber)
	{
		int clientAddressSize = sizeof(struct sockaddr_in);
		int clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddress, &clientAddressSize);

		if (clientSocket < 0)
		{
			printf("Error at accept !\n");
		}
		else
		{
			playerIDs[players] = clientSocket;
			players++;
		}
	}

	nonFullThread = 0;

	while (1)
	{
		printf("Remained players: %d\n", players);
		if (startOfTheGame == 1)
		{
			char firstLetter;

			char message1[100] = "Choose a letter and choose the first word ! You are player ";
			char buffer[5];
			sprintf(buffer,"%d",playerTurn);
			strcat(message1, buffer);

			if (write(playerIDs[playerTurn], message1, strlen(message1) * sizeof(char)) < 0)
			{
				printf("Can't send data to client !\n");
			}

			for (int i = 1; i < playersNumber; i++)
			{
				char message2[100] = "Please wait your turn ! You are player ";
				char buffer[5];
				sprintf(buffer,"%d",i);
				strcat(message2, buffer);

				if (write(playerIDs[i], message2, strlen(message1) * sizeof(char)) < 0)
				{
					printf("Can't send data to client !\n");
				}
			}

			if (read(playerIDs[playerTurn], &firstLetter, sizeof(char)) < 0)
			{
				printf("Can't read data from client !\n");
			}

			if (read(playerIDs[playerTurn], word, 100) < 0)
			{
				printf("Can't read data from client !\n");
			}

			int size = 100;
			char line[100];
			int validWord = 0;

			fseek(dictionar, 0, 0);

			while (fgets(line, size, dictionar))
			{
				line[strlen(line) - 1] = '\0';
				if (strcmp(word, line) == 0)
				{
					validWord = 1;
				}
			}

			printf("First Word: %s\n", word);

			if (write(playerIDs[playerTurn], &validWord, sizeof(int)) < 0)
			{
				printf("Can't send data to client !\n");
			}

			if (validWord == 0)
			{
				playerIDs[playerTurn] = -1;
				players--;
			}

			playerTurn++;
			startOfTheGame = 0;
		}
		else if (startOfTheGame == 0)
		{
			char* message1 = "Choose a word !";

			if (write(playerIDs[playerTurn], message1, 100) < 0)
			{
				printf("Can't send data to client !\n");
			}

			for (int i = 0; i < playersNumber && playerIDs[i] != -1 && i != playerTurn; i++)
			{
				char* message2 = "Please wait your turn !";

				if (write(playerIDs[i], message2, 100) < 0)
				{
					printf("Can't send data to client !\n");
				}
			}

			char lastTwoChars[3];
			lastTwoChars[0] = word[strlen(word) - 2];
			lastTwoChars[1] = word[strlen(word) - 1];
			lastTwoChars[2] = '\0';

			if (write(playerIDs[playerTurn], lastTwoChars, sizeof(char) * strlen(lastTwoChars))< 0)
			{
				printf("Can't send data to client !\n");
			}

			if (read(playerIDs[playerTurn], word, 100) < 0)
			{
				printf("Can't read data from client !\n");
			}


			int size = 100;
			char line[100];
			int validWord = 0;

			fseek(dictionar, 0, 0);

			if (lastTwoChars[0] != word[0] || lastTwoChars[1] != word[1])
			{
				validWord = -1;
			}

			if (validWord != -1)
			{
				while (fgets(line, size, dictionar))
				{
					line[strlen(line) - 1] = '\0';
					if (strcmp(word, line) == 0)
					{
						printf("Found ! %s\n", line);
						validWord = 1;
					}
				}
			}
			else 
			{
				validWord = 0;
			}
			
			if (write(playerIDs[playerTurn], &validWord, sizeof(int)) < 0)
			{
				printf("Can't send data to client !\n");
			}

			if (validWord == 0)
			{
				playerIDs[playerTurn] = -1;
				players--;

				if (players == 1)
				{
					for (int i = 0 ; i < playersNumber; i++)
					{
						if (playerIDs[i] != -1)
						{
							char message[100] = "You won !";
							message[strlen(message)] = '\0';
							write(playerIDs[i], message, 100);
						}
					}
					return ;
				}
			}

			playerTurn++;

			if (playerTurn >= players)
			{
				playerTurn = 0;
			}
		}
	}
}